$(document).ready(function() {

    //creating empty variables for each fields to track
    //if any field is empty then there is an error 
    var fname = '';
    var lname = '';
    var gender = '';
    var fvfood = '';
    var school = '';
    var fvtime = '';

    //validating first name
    $("#fname").keyup(function() {

        var vall = $(this).val();
        if (vall == "") {
            $('#fnameError').addClass('has-error');
            $(".fnamedanger").html("");
            $(".fnamesucess").html("");
            //make sure where there is an error set it  empty
            fname = '';
        } else if (vall.length < 3) {
            $('#fnameError').addClass('has-error');
            $(".fnamedanger").html("More than 2 Characters");
            $(".fnamesucess").html("");
            //make sure where there is an error set it  empty
            fname = '';
        } else {
            $('#fnameError').removeClass('has-error');
            $('#fnameError').addClass('has-success');
            $(".fnamedanger").html("");
            $(".fnamesucess").html("Awesome");
            //if there is no error store the value
            fname = vall;
            console.log(vall);
        }
    });

    // last name validation
    $("#lname").keyup(function() {

        var vall = $(this).val();
        if (vall == "") {
            $("#lnameError").addClass('has-error');
            $(".lnameSuccess").hide();
            lname = '';

        } else if (vall.length < 3) {
            $("#lnameError").addClass('has-error');
            $(".lnameSuccess").hide();
            lname = '';
        } else {

            $('#lnameError').removeClass('has-error');
            $('#lnameError').addClass('has-success');
            $(".lnameSuccess").show().html("Voila");
            lname = vall;
        }
    });

    // gender validation   
    $("#male").click(function() {
        gender = $(this).val();
        alert("You are : " + gender);
        //console.log(gender);
    });
    $("#female").click(function() {
        gender = $(this).val();
        alert("You are : " + gender);
    });


    /* Showing the checkbox checked item as well as unchecked */
    //change or click will do same
    $("#checkboxlist :checkbox").change(function(event) {
        if ($(this).is(':checked')) {
            alert("You have checked: " + $(this).val());
        } else {
            alert("You have unchecked: " + $(this).val());
        }
    });

    /* Favourite food validation */
    $("#form_submit").click(function() {
        getFavouriteFood();
    });

    function getFavouriteFood() {
        var foods = [];

        $("#checkboxlist input:checked").each(function() {
            foods.push($(this).val());
        });

        var checked;
        checked = foods.join(',');

        if (checked.length > 1) {
            //alert("You have checked " + checked); 
            fvfood = checked;
            console.log(fvfood);
        } else {
            alert("Please at least one of the checkbox");
            fvfood = '';
        }
    }

    //Select Your School
    $("select#selectSchool").change(function() {
        var vall = $("#selectSchool option:selected").val();
        console.log(vall);
        if (vall == '') {
            alert("You have to select any school");
            school = '';
        } else {
            school = vall;
            //console.log(school);
            alert("You have selected the School - " + school);
        }
    });

    /* Favourite time selection */
    //debug wheter it returns array or not
    var items = $("select#fvtime option").length;
    //select#fvtime if you put this without option what happens see
    $("select#fvtime option").click(function() {
        if ($(this).is(':selected')) {
            alert("You have selected: " + $(this).val());
        } else {
            alert("You have unselected: " + $(this).val());
        }
    });

    $("#selectlist").click(function(event) {
        event.preventDefault();
        getFavouriteTime();
    });    
    // function getFavouriteTime(){
    //     var fvtime = [];
        

    //     $("#selectlist select#fvtime option:selected").each(function() {
    //         fvtime.push($(this).val());
    //     });

    //     var selected;
    //     selected = fvtime.join(',');

    //     if(selected.length > 1){
    //         //alert("You have selected " + selected);
    //         fvtime = selected; 
    //     }else{
    //         fvtime = '';
    //         alert("Please at least one of the checkbox");   

    //     }
    // }  

    //form submitting if everything is okay
    $("#form_submit").click(function(event) {
        event.preventDefault();
        if (fname == "" || lname == "" || gender == "" || fvfood == "" || school == "" || fvtime == "") {
            $('#formError').html('Correct the errors dude');
        } else {
            alert('Voila');
        }
    });

});
